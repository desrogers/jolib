package jolib;

import java.util.ArrayList;

public class Calculator {
    private int total = 0;

    public int add(String s) throws Exception {
        int sum = 0;
        String nums = s;
        char delimiter = ',';

        if (s.isBlank()) return total;

        if (s.charAt(0) == '/') {
            delimiter = s.charAt(2);
            nums = s.substring(4);
            nums = nums.replace(delimiter, ',');
        }

        String[] args = nums.split("[,\n]");

        ArrayList<String> negatives = new ArrayList<>();
        for (String arg : args) {
            if (Integer.parseInt(arg) < 0) {
                negatives.add(arg);
            }
            if (arg != "") sum += Integer.parseInt(arg);
        }
        if (negatives.size() > 0) {
            throw new Exception("negatives not allowed: " + String.join(String.valueOf(delimiter), negatives));
        }
        return total += sum;
    }
}
