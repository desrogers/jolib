package jolib;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FizzBuzzTest {
    FizzBuzz fizzBuzz = new FizzBuzz();

    @Test
    public void testDivisibleByThreeReturnsFizz() {
        String expected = "Fizz";

        String actual = fizzBuzz.solve(6);
        assertEquals("should return Fizz given 6", expected, actual);
    }

    @Test
    public void testDivisibleByFiveReturnsBuzz() {
        String expected = "Buzz";
        assertEquals("should return Buzz given 10", expected, fizzBuzz.solve(10));
    }

    @Test
    public void testDivisibleByThreeAndFiveReturnsFizzBuzz() {
        String expected = "FizzBuzz";
        assertEquals("should return FizzBuzz given 15", expected, fizzBuzz.solve(15));
    }

    @Test
    public void testIfNotDivisibleReturnsNumAsString() {
        assertEquals("should return '1' given 1", "1", fizzBuzz.solve(1));
        assertEquals("should return '7' given 7","7", fizzBuzz.solve(7));
    }
}
