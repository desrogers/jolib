package jolib;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CalculatorTest {

    @Test
    public void testAddReturnsZeroGivenEmptyString()  {
        Calculator calc = new Calculator();
        int expected = 0;
        int actual = 0;
        try {
            actual = calc.add("");
            assertEquals("should return 0 given empty string", expected, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddReturnsSumAsIntegerGivenOneInput() {
        Calculator calc = new Calculator();
        int expected = 12;
        int actual = 0;
        try {
            actual = calc.add("12");
            assertEquals("should return 12 given '12'", expected, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddReturnsTotalSumGivenOneInput() {
        Calculator calc = new Calculator();
        int expected = 12;
        try {
            calc.add("6");
            int actual = calc.add("6");
            assertEquals("should return 12 after adding 6 twice", expected, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddReturnsTotalSumGivenTwoInputs() {
        Calculator calc = new Calculator();
        int expected = 27;
        int actual = 0;
        try {
            actual = calc.add("21,6");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("should return 27 given 21 and 6", expected, actual);
    }

    @Test
    public void testAddReturnsSumGivenThreeInputs() {
        Calculator calc = new Calculator();
        int expected = 3;
        int actual = 0;
        try {
            actual = calc.add("1,1,1");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("should return 3", expected, actual);
    }

    @Test
    public void testAddReturnsSumGivenFourInputs() {
        Calculator calc = new Calculator();
        int expected = 4;
        int actual = 0;
        try {
            actual = calc.add("1,1,1,1");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("should return 4", expected, actual);
    }

    @Test
    public void testAddReturnsSumGivenEightInputs() {
        Calculator calc = new Calculator();
        int expected = 8;
        int actual = 0;
        try {
            actual = calc.add("1,1,1,1,1,1,1,1");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("should return 8", expected, actual);
    }

    @Test
    public void testAddReturnsSumWithNewLineAsDelimiter() {
        Calculator calc = new Calculator();
        int expected = 8;
        int actual = 0;
        try {
            actual = calc.add("3\n2,3");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("should return 8 given new line delimiter", expected, actual);
    }

    @Test
    public void testAddReturnsSumWithOptionalDefaultDelimiter() {
        Calculator calc = new Calculator();
        int expected = 3;
        int actual = 0;
        try {
            actual = calc.add("//;\n1;2");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("should return 3 given default delimiter", expected, actual);
    }

    @Test
    public void testAddReturnsSumWithOptionalDefaultDelimiterTab() {
        Calculator calc = new Calculator();
        int expected = 6;
        int actual = 0;
        try {
            actual = calc.add("//\t\n1\t2\n3");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("should return 6 given default delimiter", expected, actual);
    }
    @Test
    public void testAddThrowsWhenSuppliedNegativeNumber() {
        Calculator calc = new Calculator();
        try {
            calc.add("-4");
            fail("The method did not throw an exception");
        } catch (Exception e) {
            assertEquals("negatives not allowed: -4", e.getMessage());
        }
    }
    @Test
    public void testAddThrowsWhenSuppliedManyNegativeNumbers() {
        Calculator calc = new Calculator();
        try {
            calc.add("-4,-6,-7");
            fail("The method did not throw an exception");
        } catch (Exception e) {
            assertEquals("negatives not allowed: -4,-6,-7", e.getMessage());
        }
    }
    @Test
    public void testAddThrowsWhenSuppliedManyNegativeNumbersWithValidNumbers() {
        Calculator calc = new Calculator();
        try {
            calc.add("1,-4,-6,-7");
            fail("The method did not throw an exception");
        } catch (Exception e) {
            assertEquals("negatives not allowed: -4,-6,-7", e.getMessage());
        }
    }
}
